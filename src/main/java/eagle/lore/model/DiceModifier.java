package eagle.lore.model;

public class DiceModifier {
    private final Operator operator;
    private final int value;

    private DiceModifier(Operator operator, int value) {
        this.operator = operator;
        this.value = value;
    }

    public Operator getOperator() {
        return operator;
    }

    public int getValue() {
        return value;
    }

    public static DiceModifier of(Operator op, int value) {
        return new DiceModifier(op, value);
    }

    public enum Operator {
        PLUS('+'), MINUS('-'), MULTIPLY('*'), DEVIDE('/');

        private char sign;

        Operator(char sign) {
            this.sign = sign;
        }

        public char getSign() {
            return sign;
        }

        public static Operator of(char op) {
            switch (op) {
                case '-':
                    return MINUS;
                case '*':
                    return MULTIPLY;
                case '/':
                    return DEVIDE;
                default:
                    return PLUS;
            }
        }
    }
}
