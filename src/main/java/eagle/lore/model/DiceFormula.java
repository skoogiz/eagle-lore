package eagle.lore.model;

import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DiceFormula {

    public static final String DICE_FORMULA_REGEXP = "^\\d+(D|T){1}\\d+?([\\+\\-\\*\\/]{1}\\d+)?$";
    public static final String OPERATION_REGEXP = "[\\+\\-\\*\\/]{1}";
    public static final String DICE_REGEXP = "(D|T){1}";
    public static final Pattern OPERATION_PATTERN = Pattern.compile(OPERATION_REGEXP);

    private final int numberOfDiceRolls;
    private final Dice dice;
    private DiceModifier modifier;

    private DiceFormula(int numberOfDiceRolls, Dice dice, DiceModifier modifier) {
        this.numberOfDiceRolls = numberOfDiceRolls;
        this.dice = dice;
        this.modifier = modifier;
    }

    public int getNumberOfDiceRolls() {
        return numberOfDiceRolls;
    }

    public Dice getDice() {
        return dice;
    }

    public Optional<DiceModifier> getModifier() {
        return Optional.ofNullable(modifier);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder()
                .append(numberOfDiceRolls)
                .append("T")
                .append(dice.getSides());
        getModifier().ifPresent(mod ->
                sb.append(mod.getOperator().getSign()).append(mod.getValue())
        );
        return sb.toString();
    }

    public static DiceFormula of(int numberOfDiceRolls, Dice dice, DiceModifier modifier) {
        return new DiceFormula(numberOfDiceRolls, dice, modifier);
    }

    public static DiceFormula of(int numberOfDiceRolls, Dice dice) {
        return new DiceFormula(numberOfDiceRolls, dice, null);
    }

    public static DiceFormula parse(String formula) {
        if (formula.toUpperCase().matches(DICE_FORMULA_REGEXP))
        {
            int nr;
            int sides;

            Matcher matcher = OPERATION_PATTERN.matcher(formula);
            boolean hasModifier = matcher.find();
            int operationIndex = hasModifier ? matcher.start() : formula.length();

            DiceModifier modifier = null;
            if (hasModifier) {
                DiceModifier.of(
                        DiceModifier.Operator.of(formula.charAt(operationIndex)),
                        Integer.parseInt(formula.substring(operationIndex + 1))
                );
            }

            String[] values = formula.substring(0, operationIndex).split(DICE_REGEXP);
            nr = Integer.parseInt(values[0]);
            sides = Integer.parseInt(values[1]);

            return of(nr, new Dice(sides), modifier);
        }
        else
        {
            throw new IllegalArgumentException("Illegal formula '" + formula + "'!");
        }
    }
}
